

# Podgląd komponentu karty NFT

<div>
         <img src="design/mobile-design.jpg" height="500px"/>
</div>

To wyzwanie HTML & CSS, które doskonale nadaje się dla każdego, kto dopiero zaczyna, lub dla każdego, kto chce się pobawić małym projektem. Głównym zadaniem jest zbudowanie tego komponentu karty podglądu i nadanie mu wyglądu jak najbardziej zbliżonego do projektu.

> 💡 Można używać dowolnych narzędzi, które pomogą nam ukończyć wyzwanie.



## Układ

Projektowane były zgodnie z następującymi szerokościami:

- Telefon komórkowy: 375px
- Komputer stacjonarny: 1440px

> 💡 Te wartości to tylko rozmiary projektu. Upewnij się, że zawartość jest responsywna i spełnia wymagania WCAG, testując pełny zakres szerokości ekranów od 320px do dużych ekranów.

## Kolory

### Główne

- Soft blue: hsl(215, 51%, 70%)
- Cyan: hsl(178, 100%, 50%)

### Neutralne

- Very dark blue (main BG): hsl(217, 54%, 11%)
- Very dark blue (card BG): hsl(216, 50%, 16%)
- Very dark blue (line): hsl(215, 32%, 27%)
- White: hsl(0, 0%, 100%)

## Typografia

### Body Copy

- Font size (paragraph): 18px

### Font

- Family: [Outfit](https://fonts.google.com/specimen/Outfit)
- Weights: 300, 400, 600

## Strony wykorzystane w projekcie

### Wideo - MMC School

[MMC School ×Szybki Kurs HTML & CSS!](https://www.youtube.com/watch?v=EAfiIuQZajs)

<div>
      <a href="https://www.youtube.com/watch?v=EAfiIuQZajs">
         <img src="https://i.ytimg.com/vi/EAfiIuQZajs/maxresdefault.jpg" width="400px"/>
      </a>
</div>

### Strona Frontend Mentor

[MMC School × NFT preview card component](https://www.frontendmentor.io/challenges/nft-preview-card-component-SbdUL_w0U)

### Strona MDN Web Docs

[MDN - Developer Mozilla](https://developer.mozilla.org/en-US/docs/Web/CSS/Reference)

### Strona Google Fonts "Outfit"

[Outfit - google fonts](https://fonts.google.com/specimen/Outfit)  